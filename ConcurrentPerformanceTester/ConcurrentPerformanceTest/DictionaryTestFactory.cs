﻿// Concurrent Hash Dictionary Test Framework - a framework for testing the Concurrent Hash Dictionary
// 
// Copyright(C) 2016 Edgars Ankorins & Mihai-Marius Vlasceanu
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;

namespace ConcurrentPerformanceTest
{
    /// <summary>
    /// Factory for tests
    /// </summary>
    public class DictionaryTestFactory
    {
        /// <summary>
        /// Generates all the test cases as a List
        /// </summary>
        /// <param name="dictionary">Dictionary of K -> V pairs</param>
        /// <param name="noOfThreads">Number of workers/threads</param>
        /// <returns></returns>
        public static List<TestRun> RunComprehensiveTests(TestSettings settings)
        {
            // Holds the list of all tests
            List<TestRun> testRuns = new List<TestRun>();

            // Tests with 100% reads
            List<DictionaryTest> dictReadOnly = GenerateTests(settings, 1);

            // Tests with 80% reads
            List<DictionaryTest> dictHeavyRead = GenerateTests(settings, 0.8);

            // Tests with 50% reads and writes
            List<DictionaryTest> dictEvenReadWrite = GenerateTests(settings, 0.5);

            // Tests with 80% writes
            List<DictionaryTest> dictHeavyWrite = GenerateTests(settings, 0.2);

            // Tests with 100% writes
            List<DictionaryTest> dictOnlyWrite = GenerateTests(settings, 0.0);

            // Add all tests to the runner
            testRuns.Add(RunTest(dictReadOnly, settings));
            testRuns.Add(RunTest(dictHeavyRead, settings));
            testRuns.Add(RunTest(dictEvenReadWrite, settings));
            testRuns.Add(RunTest(dictHeavyWrite, settings));
            testRuns.Add(RunTest(dictOnlyWrite, settings));


            return testRuns;
        }

        public static List<TestRun> RunResizeTest(TestSettings settings)
        {
            List<TestRun> testRuns = new List<TestRun>();
            // Generate a range of worker threads
            ICollection<int> threadCounts = Enumerable.Range(1, settings.NoOfThreads).ToList();

            // The list of tests
            List<DictionaryTest> tests = new List<DictionaryTest>();

            // Add a new test to be executed on the dictionary foreach worker
            foreach (var threads in threadCounts)
            {

                tests.Add(new DictionaryTest(settings, threads, 0.0));
            }
            // Initiate a run
            var testRun = new TestRun(tests.First().Dictionary.DictName, tests.First().ReadWriteRatio, settings);
            testRun.TestResults = tests.Select(t => new TestResult(t.NoOfThreads, t.ResizeTest())).ToList();

            // Print to console
            Console.WriteLine(testRun.ToCSV());
            testRuns.Add(testRun);
            return testRuns;
        } 
        /// <summary>
        /// Runs tests with 100% reads
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public static List<TestRun> RunReadsTest(TestSettings settings)
        {
            // Holds the list of all tests
            List<TestRun> testRuns = new List<TestRun>();

            // Tests with 80% reads
            List<DictionaryTest> dictReadsOnly = GenerateTests(settings, 1.0);

            // Add the test
            testRuns.Add(RunTest(dictReadsOnly, settings));

            return testRuns;
        }

        /// <summary>
        /// Runs tests with 100% writes
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public static List<TestRun> RunWritesTest(TestSettings settings)
        {
            // Holds the list of all tests
            List<TestRun> testRuns = new List<TestRun>();

            // Tests with 80% reads
            List<DictionaryTest> dictWriteOnly = GenerateTests(settings, 0.0);

            // Add the test
            testRuns.Add(RunTest(dictWriteOnly, settings));

            return testRuns;
        }

        /// <summary>
        /// Generates tests based threads and read/write ratio
        /// </summary>
        /// <param name="dictionary">The K -> V dictionary</param>
        /// <param name="noOfThreads">Number of workers</param>
        /// <param name="readWriteRatio">The read/write ratio</param>
        /// <returns></returns>
        public static List<DictionaryTest> GenerateTests(TestSettings settings, double readWriteRatio)
        {
            // Generate a range of worker threads
            ICollection<int> threadCounts = Enumerable.Range(1, settings.NoOfThreads).ToList();

            // The list of tests
            List<DictionaryTest> tests = new List<DictionaryTest>();

            // Add a new test to be executed on the dictionary foreach worker
            foreach (var threads in threadCounts) {
                
                tests.Add(new DictionaryTest(settings, threads, readWriteRatio));
            }

            return tests;
        }
        
        /// <summary>
        /// Runs all the tests in the list
        /// </summary>
        /// <param name="tests">A List of tests</param>
        /// <returns>An instance of a ran test</returns>
        public static TestRun RunTest(List<DictionaryTest> tests, TestSettings settings)
        {
            // Initiate a run
            TestRun testRun = new TestRun(tests.First().Dictionary.DictName, tests.First().ReadWriteRatio, settings);
            testRun.TestResults = tests.Select(t => new TestResult(t.NoOfThreads, t.Run())).ToList();

            // Print to console
            Console.WriteLine(testRun.ToCSV());

            return testRun;
        }
    }
}
