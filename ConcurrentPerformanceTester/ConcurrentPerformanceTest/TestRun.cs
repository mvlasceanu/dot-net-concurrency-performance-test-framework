﻿// Concurrent Hash Dictionary Test Framework - a framework for testing the Concurrent Hash Dictionary
// 
// Copyright(C) 2016 Edgars Ankorins & Mihai-Marius Vlasceanu
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

namespace ConcurrentPerformanceTest
{
    public class TestRun
    {
        /// <summary>
        /// Dictionary class name
        /// </summary>
        public string DictionaryType { get; set; }

        /// <summary>
        /// Read/write ratio
        /// </summary>
        public double ReadWriteRatio { get; set; }

        /// <summary>
        /// Test results
        /// </summary>
        public ICollection<TestResult> TestResults { get; set; }

        /// <summary>
        /// Holds run settings
        /// </summary>
        private TestSettings settings;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dictionaryType">Dictionary class name</param>
        /// <param name="readWriteRatio">Read/write ratio</param>
        public TestRun(string dictionaryType, double readWriteRatio, TestSettings settings)
        {
            DictionaryType = dictionaryType;
            ReadWriteRatio = readWriteRatio;
            Settings = settings;
            TestResults = new List<TestResult>();
        }

        /// <summary>
        /// Adds result of the test to the tests collection
        /// </summary>
        /// <param name="result"></param>
        public void AddResult(TestResult result)
        {
            TestResults.Add(result);
        }

        /// <summary>
        /// Test Run settings
        /// </summary>
        public TestSettings Settings
        {
            get { return settings; }
            set { settings = value; }
        }

        /// <summary>
        /// Results in CSV format
        /// </summary>
        /// <returns>CSV-like text</returns>
        public string ToCSV()
        {   
            string result = "";
            foreach (var item in TestResults)
            {
                result += string.Format("{0},{1},{2},{3}{4}", DictionaryType, ReadWriteRatio, item.NoOfThreads,
                    item.AverageRunTime, Environment.NewLine);
            }
            return result;
        }

        /// <summary>
        /// Override for the ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("Test Run for {0}, with ReadWrite: {1}", DictionaryType, ReadWriteRatio);
        }
    }

    /// <summary>
    /// Result of a test
    /// </summary>
    public class TestResult
    {

        /// <summary>
        /// Numberof threads used
        /// </summary>
        public int NoOfThreads { get; set; }

        /// <summary>
        /// The average run time
        /// </summary>
        public double AverageRunTime { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="noOfThreads"></param>
        /// <param name="averageRunTime"></param>
        public TestResult(int noOfThreads, double averageRunTime)
        {
            NoOfThreads = noOfThreads;
            AverageRunTime = averageRunTime;
        }

        /// <summary>
        /// Override
        /// </summary>
        /// <returns>String representation of an instance</returns>
        public override string ToString()
        {
            return string.Format("Threads: {0} Runtime: {1}", NoOfThreads, AverageRunTime);
        }
    }
}
