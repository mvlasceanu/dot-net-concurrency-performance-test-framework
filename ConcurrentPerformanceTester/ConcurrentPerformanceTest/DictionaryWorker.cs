﻿// Concurrent Hash Dictionary Test Framework - a framework for testing the Concurrent Hash Dictionary
// 
// Copyright(C) 2016 Edgars Ankorins & Mihai-Marius Vlasceanu
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Linq;

namespace ConcurrentPerformanceTest
{
    /// <summary>
    /// Worker object
    /// </summary>
    public class DictionaryWorker
    {
        /// <summary>
        /// Per worker unique ID
        /// </summary>
        private int id;

        /// <summary>
        /// Disctionary to work on
        /// </summary>
        private ITestableDict<int, Entry> dictionary;

        /// <summary>
        /// Thread number and the number of operations to perform
        /// </summary>
        private int threadNo, noOfOperations, noOfThreads;

        /// <summary>
        /// Read/write operations ratio
        /// </summary>
        private double readWriteRatio;

        /// <summary>
        /// Holds the number if reads, writes and deletes performed during work
        /// </summary>
        public int reads, writes, deletes;


        public int failedReads, failedWrites, failedDeletes;
        /// <summary>
        /// Operations results
        /// </summary>
        public int[] threadOperations;

        /// <summary>
        /// Keyspace to work on
        /// </summary>
        private int[] keySpace;


        private Entry[] valueSpace;
        /// <summary>
        /// The constructor (doh!)
        /// </summary>
        /// <param name="dictionary"><k,V> dictionary to work on</k></param>
        /// <param name="rangeStart">Key space start</param>
        /// <param name="rangeEnd">Key space end</param>
        /// <param name="keySpace">The keyspace</param>
        /// <param name="noOfOperations">Total number of operations to be performed</param>
        /// <param name="readWriteRatio">Read/write ratio</param>
        /// <param name="threadNo">Thread number</param>
        /// <param name="noOfThreads">Number of threads</param>
        public DictionaryWorker(ITestableDict<int, Entry> dictionary, int[] keySpace, int noOfOperations, double readWriteRatio, int threadNo, int noOfThreads)
        {
            Random rand = new Random(35123123 * threadNo);
            this.dictionary = dictionary;
            this.threadNo = threadNo;
            this.noOfOperations = noOfOperations;
            this.noOfThreads = noOfThreads;
            this.readWriteRatio = readWriteRatio;
            threadOperations = new int[noOfThreads + 1];
            threadOperations[noOfThreads] = keySpace.Length;
            this.keySpace = keySpace.OrderBy(i => rand.Next()).ToArray();
            this.valueSpace = this.keySpace.Select(k => new Entry { ThreadId = threadNo, Key = k}).ToArray();
        }

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Work, mexican work!
        /// </summary>
        public void DoWork()
        {
            // Random key space
            Random rand = new Random(335531 * threadNo);
            
            for (int i = 0; i < noOfOperations; i++)
            {
                int index = i % keySpace.Length;
                // Pick random key
                int key = keySpace[index];

                // Read or write or delete ?
                TaskType type = DetermineTask(rand.NextDouble());
                
                // Do it!
                if (type == TaskType.READ)
                {
                    //DateTime startRead = Util.startOperationTimer(this, TaskType.READ);
                    Entry val = dictionary.Get(key);

                    if (val == null)
                    {
                        failedReads++;
                    }
                    else
                    {
                        reads++;
                    }
                    //TimeSpan spanRead = Util.stopOperationTimer(startRead, this, TaskType.READ);
                }
                else if (type == TaskType.WRITE)
                {
                    Entry value = valueSpace[index];
                    Entry v = dictionary.AddOrUpdate(key, value);
                    if(v == null)
                    {
                        failedWrites++;
                        continue;
                    }
                    //string threadString = v.Substring(0, 2);
                    int thread = v.ThreadId;
                    if (thread >= 0)
                    {
                        threadOperations[thread]--;
                    }
                    else
                    {
                        threadOperations[noOfThreads]--;
                    }
                    threadOperations[threadNo]++;
                    //TimeSpan spanWrite = Util.stopOperationTimer(startWrite, this, TaskType.READ);
                    writes++;
                }
                else
                {
                    //DateTime startDelete = Util.startOperationTimer(this, TaskType.READ);
                    Entry v;
                    bool deleted = dictionary.Delete(key, out v);

                    if (deleted)
                    {
                        //int threadD = Convert.ToInt32(v.Substring(0, 2));
                        int threadD = v.ThreadId;
                        if (threadD >= 0)
                        {
                            threadOperations[threadD]--;
                        }
                        else
                        {
                            threadOperations[noOfThreads]--;
                        }
                        threadOperations[threadNo]++;

                        Entry value = valueSpace[index];
                        dictionary.AddOrUpdate(key, value);

                        //TimeSpan spanDelete = Util.stopOperationTimer(startDelete, this, TaskType.READ);
                        deletes++;

                    }
                    else
                    {
                        failedDeletes++;
                    }
                }
            }
            threadOperations[0]++;
        }

        public void DoResize()
        {
            
        }

        /// <summary>
        /// Determines what action should be performed on the dictionary
        /// </summary>
        /// <param name="rnd"></param>
        /// <returns></returns>
        private TaskType DetermineTask(double rnd)
        {
            if (rnd <= readWriteRatio)
            {
                return TaskType.READ;
            }
            else if (0.05 >= rnd - readWriteRatio)
            {
                return TaskType.DELETE;
            }
            else
            {
                return TaskType.WRITE;
            }
        }

        /// <summary>
        /// Helper for actions
        /// </summary>
        public enum TaskType
        {
            READ,
            WRITE,
            DELETE
        }
    }
}