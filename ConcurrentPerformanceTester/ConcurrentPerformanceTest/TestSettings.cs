﻿// Concurrent Hash Dictionary Test Framework - a framework for testing the Concurrent Hash Dictionary
// 
// Copyright(C) 2016 Edgars Ankorins & Mihai-Marius Vlasceanu
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Linq;

namespace ConcurrentPerformanceTest
{
    /// <summary>
    /// Parameters for tests
    /// </summary>
    [Serializable]
    public class TestSettings
    {
        /// <summary>
        /// Number of threads to run
        /// </summary>
        private int noOfThreads;

        /// <summary>
        /// Number of operation to be perfomed
        /// </summary>
        private int noOfOperations;

        /// <summary>
        /// Key space
        /// </summary>
        private int noOfKeys;

        /// <summary>
        /// How many times to run
        /// </summary>
        private int noOfRuns = 5;

        /// <summary>
        /// The K -> V dictionary
        /// </summary>
        public ITestableDict<int, Entry> Dictionary { get; set; }

        /// <summary>
        /// The key space array
        /// </summary>
        private int[] keySpace;

        public TestSettings() { }

        public TestSettings(ITestableDict<int, Entry> dictionary, int noOfThreads, int noOfOperations, int noOfKeys, int noOfRuns)
        {
            this.noOfThreads = noOfThreads;
            this.noOfOperations = noOfOperations;
            this.noOfKeys = noOfKeys;
            this.noOfRuns = noOfRuns;
            Dictionary = dictionary;
            keySpace = createKeySpace(noOfKeys);

        }

        /// <summary>
        /// Key space
        /// </summary>
        public int[] KeySpace
        {
            get { return keySpace; }
            set { keySpace = value; }
        }

        /// <summary>
        /// Getter setter for noOfThreads
        /// </summary>
        public int NoOfThreads
        {
            get { return noOfThreads; }
            set { noOfThreads = value; }
        }

        /// <summary>
        /// Getter/setter for noOfOperations
        /// </summary>
        public int NoOfOperations
        {
            get { return noOfOperations; }
            set { noOfOperations = value; }
        }

        /// <summary>
        /// Getter/Setter for noOfKeys
        /// </summary>
        public int NoOfKeys
        {
            get { return noOfKeys; }
            set { noOfKeys = value; }
        }

        /// <summary>
        /// Getter/Setter for noOfRuns
        /// </summary>
        public int NoOfRuns
        {
            get { return noOfRuns; }
            set { noOfRuns = value; }
        }



        /// <summary>
        /// Generates the key space values 
        /// </summary>
        /// <param name="noOfKeys">The number of keys to be generated</param>
        /// <returns></returns>
        private int[] createKeySpace(int noOfKeys)
        {
            // Random space (seed is random)
            Random rand = new Random(1352315135);

            // Array to hold the keys
            int[] keys = new int[noOfKeys];

            // Generate them
            //int current = 2;
            for (int i = 0; i < noOfKeys; i++)
            {
                int key = rand.Next(0, int.MaxValue / 100);
                //int key = GetNextPrimeNumber(current);
                keys[i] = key;
                //current = key;
            }
            var uniques = keys.Distinct();
            return keys;
        }

        /// <summary>
        /// Calculates the next Mersenne prime number
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public int GetNextPrimeNumber(int number)
        {
            while (true)
            {
                bool isPrime = true;
                //increment the number by 1 each time
                number = number + 1;

                int squaredNumber = (int)Math.Sqrt(number);

                //start at 2 and increment by 1 until it gets to the squared number
                for (int i = 2; i <= squaredNumber; i++)
                {
                    if (number % i == 0)
                    {
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime)
                {
                    return number;
                }
            }
        }
    }
}
