﻿// Concurrent Hash Dictionary Test Framework - a framework for testing the Concurrent Hash Dictionary
// 
// Copyright(C) 2016 Edgars Ankorins & Mihai-Marius Vlasceanu
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

namespace ConcurrentPerformanceTest
{
    /// <summary>
    /// Interface to be implemented by each dictionary test class
    /// that shall be tested by this framework
    /// </summary>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="V"></typeparam>
    public interface ITestableDict<K, V>
    {

        string DictName { get; }
        /// <summary>
        /// ContainsKey method
        /// </summary>
        /// <param name="k">Key to be looked for</param>
        /// <returns>Does the dictionary contain key K</returns>
        bool ContainsKey(K k);

        /// <summary>
        /// Get value V at key K
        /// </summary>
        /// <param name="k">Key to get the value at</param>
        /// <returns>The value</returns>
        V Get(K k);

        /// <summary>
        /// Add value V at key K or update it
        /// </summary>
        /// <param name="k">Key to Add/Update</param>
        /// <param name="v">Value to be added/changed</param>
        /// <returns>The updated value</returns>
        V AddOrUpdate(K k, V v);

        /// <summary>
        /// Delete the entry at key K
        /// </summary>
        /// <param name="k">Key to be deleted</param>
        /// <param name="v">The out value to pass the deleted entry to</param>
        /// <returns>Has the key been deleted?</returns>
        bool Delete(K k, out Entry v);

        /// <summary>
        /// Empty the dictionary
        /// </summary>
        void Clear();

        /// <summary>
        /// Count the items in the dictionary
        /// </summary>
        /// <returns>An integer representing the number of entries</returns>
        int Count();

        /// <summary>
        /// List the keys in the dictionary
        /// </summary>
        /// <returns>An array containing all the keys in the dictionary</returns>
        K[] Keys();
    }
}
