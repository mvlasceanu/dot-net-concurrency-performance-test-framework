﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConcurrentPerformanceTest
{
    // For weeks 7 and 9
    // sestoft@itu.dk * 2014-10-28, 2015-02-15, 2015-02-25, 2015-04-28

    // HANDOUT VERSION with some methods deleted

    using System;
    using System.Threading;         // Barrier, Thread, Volatile
    using System.Collections.Concurrent;
    using System.Collections.Generic;       // for KeyValuePair

    public class Tests
    {
        public static void Assert(bool b)
        {
            if (!b)
                throw new Exception(String.Format("ERROR: Assertion failure"));
        }

        public static void Assert(bool b, String msg)
        {
            if (!b)
                throw new Exception(String.Format("ERROR: Assertion failure: {0}", msg));
        }
    }

    public class TestStripedMap : Tests
    {
        public static void Main(String[] args)
        {
            SystemInfo();
            testAllMaps();
            exerciseAllMaps();
            // timeAllMaps();
        }

        private static void timeAllMaps()
        {
            int bucketCount = 100000, lockCount = 32;
            for (int t = 1; t <= 32; t++)
            {
                int threadCount = t;
                Mark7(String.Format("{0,-21} {1}", "SynchronizedMap", threadCount),
                    (int i) => {
                        return timeMap(threadCount,
                             new SynchronizedMap<int, String>(bucketCount));
                    });
                Mark7(String.Format("{0,-21} {1}", "StripedMap", threadCount),
                    (int i) => {
                        return timeMap(threadCount,
                             new StripedMap<int, String>(bucketCount, lockCount));
                    });
                Mark7(String.Format("{0,-21} {1}", "StripedWriteMap", threadCount),
                    (int i) => {
                        return timeMap(threadCount,
                             new StripedWriteMap<int, String>(bucketCount, lockCount));
                    });
                Mark7(String.Format("{0,-21} {1}", "WrapConcHashMap", threadCount),
                    (int i) => {
                        return timeMap(threadCount,
                             new WrapConcurrentHashMap<int, String>());
                    });
            }
        }

        private static double timeMap(int threadCount, OurMap<int, String> map)
        {
            int iterations = 5000000, perThread = iterations / threadCount;
            int range = 200000;
            return exerciseMap(threadCount, perThread, range, map);
        }

        private static double exerciseMap(int threadCount, int perThread, int range,
                                          OurMap<int, String> map)
        {
            Thread[] threads = new Thread[threadCount];
            for (int t = 0; t < threadCount; t++)
            {
                int myThread = t;
                threads[t] = new Thread(delegate () {
                    Random random = new Random(37 * myThread + 78);
                    for (int i = 0; i < perThread; i++)
                    {
                        int key = random.Next(range);
                        if (!map.containsKey(key))
                        {
                            // Add key with probability 60%
                            if (random.NextDouble() < 0.60)
                                map.put(key, key.ToString());
                        }
                        else // Remove key with probability 2% and reinsert
                          if (random.NextDouble() < 0.02)
                        {
                            map.remove(key);
                            map.putIfAbsent(key, key.ToString());
                        }
                    }
                    int ai = 0;
                    map.forEach(delegate (int k, String v) {
                        Interlocked.Increment(ref ai);
                    });
                    // Console.WriteLine(ai + " " + map.size());
                });
            }
            for (int t = 0; t < threadCount; t++)
                threads[t].Start();
            map.reallocateBuckets();
            for (int t = 0; t < threadCount; t++)
                threads[t].Join();
            return map.size();
        }

        private static void exerciseAllMaps()
        {
            int bucketCount = 100000, lockCount = 32, threadCount = 16;
            int iterations = 1600000, perThread = iterations / threadCount;
            int range = 100000;
            Console.WriteLine(Mark7(String.Format("{0,-21} {1}", "SynchronizedMap", threadCount),
                  (int i) => {
                      return exerciseMap(threadCount, perThread, range,
                                 new SynchronizedMap<int, String>(bucketCount));
                  }));
            Console.WriteLine(Mark7(String.Format("{0,-21} {1}", "StripedMap", threadCount),
                  (int i) => {
                      return exerciseMap(threadCount, perThread, range,
                                 new StripedMap<int, String>(bucketCount, lockCount));
                  }));
            Console.WriteLine(Mark7(String.Format("{0,-21} {1}", "StripedWriteMap", threadCount),
                  (int i) => {
                      return exerciseMap(threadCount, perThread, range,
                                 new StripedWriteMap<int, String>(bucketCount, lockCount));
                  }));
            Console.WriteLine(Mark7(String.Format("{0,-21} {1}", "WrapConcHashMap", threadCount),
                  (int i) => {
                      return exerciseMap(threadCount, perThread, range,
                                 new WrapConcurrentHashMap<int, String>());
                  }));
        }

        // Very basic sequential functional test of a hash map. 

        private static void testMapSequential(OurMap<int, String> map)
        {
            Console.WriteLine("\nSequential test of {0}", map.GetType());
            Assert(map.size() == 0);
            Assert(!map.containsKey(117));
            Assert(map.get(117) == null);
            Assert(map.put(117, "A") == null);
            Assert(map.containsKey(117));
            Assert(map.get(117).Equals("A"));
            Assert(map.put(17, "B") == null);
            Assert(map.size() == 2);
            Assert(map.containsKey(17));
            Assert(map.get(117).Equals("A"));
            Assert(map.get(17).Equals("B"));
            Assert(map.put(117, "C").Equals("A"));
            Assert(map.containsKey(117));
            Assert(map.get(117).Equals("C"));
            Assert(map.size() == 2);
            map.forEach((k, v) => { Console.WriteLine("{0,10} maps to {1}", k, v); });
            Console.WriteLine();
            Assert(map.remove(117).Equals("C"));
            Assert(!map.containsKey(117));
            Assert(map.get(117) == null);
            Assert(map.size() == 1);
            Assert(map.putIfAbsent(17, "D").Equals("B"));
            Assert(map.get(17).Equals("B"));
            Assert(map.size() == 1);
            Assert(map.containsKey(17));
            Assert(map.putIfAbsent(217, "E") == null);
            Assert(map.get(217).Equals("E"));
            Assert(map.size() == 2);
            Assert(map.containsKey(217));
            Assert(map.putIfAbsent(34, "F") == null);
            map.forEach((k, v) => { Console.WriteLine("{0,10} maps to {1}", k, v); });
            Console.WriteLine();
            map.reallocateBuckets();
            Assert(map.size() == 3);
            Assert(map.get(17).Equals("B") && map.containsKey(17));
            Assert(map.get(217).Equals("E") && map.containsKey(217));
            Assert(map.get(34).Equals("F") && map.containsKey(34));
            map.forEach((k, v) => { Console.WriteLine("{0,10} maps to {1}", k, v); });
            Console.WriteLine();
            map.reallocateBuckets();
            Assert(map.remove(34).Equals("F"));
            Assert(!map.containsKey(34));
            Assert(map.size() == 2);
            Assert(map.get(17).Equals("B") && map.containsKey(17));
            Assert(map.get(217).Equals("E") && map.containsKey(217));
            map.forEach((k, v) => { Console.WriteLine("{0,10} maps to {1}", k, v); });
        }

        private static void testMapConcurrent(OurMap<int, String> map)
        {
            Console.WriteLine("\nConcurrent test of {0}", map.GetType());
            int threadCount = 24, trialCount = 2000000;
            ConcurrentTester tester = new ConcurrentTester(map, threadCount, trialCount);
            tester.RunTest();
        }

        private static void testAllMaps()
        {
            testMapSequential(new SynchronizedMap<int, String>(25));
            testMapSequential(new StripedMap<int, String>(25, 5));
            testMapSequential(new StripedWriteMap<int, String>(25, 5));
            testMapSequential(new WrapConcurrentHashMap<int, String>());
            testMapConcurrent(new SynchronizedMap<int, String>(50));
            testMapConcurrent(new StripedMap<int, String>(50, 5));
            testMapConcurrent(new StripedWriteMap<int, String>(50, 5));
            testMapConcurrent(new WrapConcurrentHashMap<int, String>());
        }

        // --- Benchmarking infrastructure ---

        // NB: Modified to show microseconds instead of nanoseconds

        public static double Mark7(String msg, Func<int, double> f)
        {
            int n = 10, count = 1, totalCount = 0;
            double dummy = 0.0, runningTime = 0.0, st = 0.0, sst = 0.0;
            do
            {
                count *= 2;
                st = sst = 0.0;
                for (int j = 0; j < n; j++)
                {
                    Timer t = new Timer();
                    for (int i = 0; i < count; i++)
                        dummy += f(i);
                    runningTime = t.Check();
                    double time = runningTime * 1e6 / count; // microseconds
                    st += time;
                    sst += time * time;
                    totalCount += count;
                }
            } while (runningTime < 0.25 && count < Int32.MaxValue / 2);
            double mean = st / n, sdev = Math.Sqrt((sst - mean * mean * n) / (n - 1));
            Console.WriteLine("{0,-25} {1,15:F1} us {2,10:F2} {3,10:D}", msg, mean, sdev, count);
            return dummy / totalCount;
        }

        private static void SystemInfo()
        {
            Console.WriteLine("# OS          {0}",
              Environment.OSVersion.VersionString);
            Console.WriteLine("# .NET vers.  {0}",
              Environment.Version);
            Console.WriteLine("# 64-bit OS   {0}",
              Environment.Is64BitOperatingSystem);
            Console.WriteLine("# 64-bit proc {0}",
              Environment.Is64BitProcess);
            Console.WriteLine("# CPU         {0}; {1} \"cores\"",
              Environment.GetEnvironmentVariable("PROCESSOR_IDENTIFIER"),
              Environment.ProcessorCount);
            Console.WriteLine("# Date        {0:s}",
              DateTime.Now);
        }
    }

    class ConcurrentTester : Tests
    {
        // We use one Barrier for both starting and stopping
        private readonly Barrier barrier;
        private readonly OurMap<int, String> map;
        private readonly int threadCount, trialCount;
        private int addedSum = 0, removedSum = 0;

        public ConcurrentTester(OurMap<int, String> map, int threadCount, int trialCount)
        {
            this.map = map;
            this.threadCount = threadCount;
            this.trialCount = trialCount;
            this.barrier = new Barrier(threadCount + 1);
        }

        public void RunTest()
        {
            const int range = 200;
            Thread[] threads = new Thread[threadCount];
            // Net number of entries added by each thread
            int[] totalCounts = new int[threadCount];
            for (int t = 0; t < threadCount; t++)
            {
                int thisThread = t;
                threads[t] = new Thread(delegate () {
                    int[] localCounts = new int[threadCount];
                    Random random = new Random();
                    int localAdded = 0, localRemoved = 0;
                    barrier.SignalAndWait(); // Wait for other threads to be ready
                    for (int i = 0; i < trialCount; i++)
                    {
                        int key = random.Next(range);
                        if (!map.containsKey(key))
                        {
                            // Add key with probability 60%
                            if (random.NextDouble() < 0.60)
                            {
                                String newVal = String.Format("{0:D3}:{1}", thisThread, key.ToString());
                                // Console.WriteLine(newVal);
                                String oldVal = map.put(key, newVal);
                                if (oldVal == null)
                                { // An entry was added
                                    localAdded += key;
                                    localCounts[thisThread]++;
                                }
                                else { // An entry was replaced
                                    int creator = Int32.Parse(oldVal.Substring(0, 3)),
                                      val = Int32.Parse(oldVal.Remove(0, 4));
                                    Assert(val == key,
                                           String.Format("add: wrong old value {0} for key {1}", val, key));
                                    localCounts[thisThread]++;
                                    localCounts[creator]--;
                                }
                            }
                        }
                        else {
                            // Remove key with probability 5% and reinsert
                            if (random.NextDouble() < 0.05)
                            {
                                {
                                    String oldVal = map.remove(key);
                                    if (null != oldVal)
                                    { // An entry was removed
                                        int val = Int32.Parse(oldVal.Remove(0, 4));
                                        Assert(val == key,
                                               String.Format("remove: wrong value {0} for key {1}", val, key));
                                        localRemoved += key;
                                        int creator = Int32.Parse(oldVal.Substring(0, 3));
                                        localCounts[creator]--;
                                    }
                                }
                                {
                                    String newVal = String.Format("{0:D3}:{1}", thisThread, key.ToString()),
                                      oldVal = map.putIfAbsent(key, newVal);
                                    if (null == oldVal)
                                    { // An entry was added
                                        localAdded += key;
                                        localCounts[thisThread]++;
                                    }
                                }
                            }
                        }
                    }
                    lock (totalCounts)
                    {
                        for (int tt = 0; tt < threadCount; tt++)
                            totalCounts[tt] += localCounts[tt];
                        this.addedSum += localAdded;
                        this.removedSum += localRemoved;
                    }
                    barrier.SignalAndWait();
                });
            }
            for (int t = 0; t < threadCount; t++)
                threads[t].Start();
            barrier.SignalAndWait();  // wait for all threads to be ready
            barrier.SignalAndWait();  // wait for all threads to finish      
            int finalSum = 0;
            int[] countsAtEnd = new int[threadCount];
            map.forEach(delegate (int k, String v) {
                finalSum += k;
                int creator = Int32.Parse(v.Substring(0, 3));
                countsAtEnd[creator]++;
                Assert(Int32.Parse(v.Remove(0, 4)) == k,
                       String.Format("wrong value {0} for key {1}", v, k));
            });
            int countsAtEndSum = 0;
            for (int t = 0; t < threadCount; t++)
            {
                countsAtEndSum += countsAtEnd[t];
                Assert(countsAtEnd[t] == totalCounts[t],
                       String.Format("counts mismatch, countsAtEnd[{0}] = {1} != totalCounts[{0}] = {2}",
                                     t, countsAtEnd[t], totalCounts[t]));
            }
            Assert(countsAtEndSum == map.size(),
                   String.Format("size mismatch, countsAtEndSum={0} != map.size()={1}",
                                 countsAtEndSum, map.size()));

            Assert(addedSum - removedSum == finalSum,
                   String.Format("sum mismatch, added={0}, removed={1}, final={2}",
                                 addedSum, removedSum, finalSum));
        }
    }

    // ----------------------------------------------------------------------
    // Java-style map (or dictionary) 

    interface OurMap<K, V> where V : class
    {
        bool containsKey(K k);
        V get(K k);
        V put(K k, V v);
        V putIfAbsent(K k, V v);
        V remove(K k);
        int size();
        void forEach(Action<K, V> consumer);
        void reallocateBuckets();
    }

    // ----------------------------------------------------------------------
    // A hashmap that permits thread-safe concurrent operations, similar
    // to a synchronized version of HashMap<K,V>.

    class SynchronizedMap<K, V> : OurMap<K, V> where V : class
    {
        // Synchronization policy: 
        //   buckets[hash] and cachedSize are guarded by this
        private ItemNode[] buckets;
        private int cachedSize;

        public SynchronizedMap(int bucketCount)
        {
            this.buckets = new ItemNode[bucketCount];
        }

        // Protect against poor hash functions and make non-negative
        private static int getHash(K k)
        {
            int kh = k.GetHashCode();
            return (kh ^ (kh >> 16)) & 0x7FFFFFFF;
        }

        // Return true if key k is in map, else false
        public bool containsKey(K k)
        {
            lock (this)
            {
                int h = getHash(k), hash = h % buckets.Length;
                return ItemNode.search(buckets[hash], k) != null;
            }
        }

        // Return value v associated with key k, or null
        public V get(K k)
        {
            lock (this)
            {
                int h = getHash(k), hash = h % buckets.Length;
                ItemNode node = ItemNode.search(buckets[hash], k);
                if (node != null)
                    return node.v;
                else
                    return null;
            }
        }

        public int size()
        {
            lock (this)
            {
                return cachedSize;
            }
        }

        // Put v at key k and return null if not present; else put new value
        // and return old value
        public V put(K k, V v)
        {
            lock (this)
            {
                int h = getHash(k), hash = h % buckets.Length;
                ItemNode node = ItemNode.search(buckets[hash], k);
                if (node != null)
                {
                    V old = node.v;
                    node.v = v;
                    return old;
                }
                else {
                    buckets[hash] = new ItemNode(k, v, buckets[hash]);
                    cachedSize++;
                    return null;
                }
            }
        }

        // Put v at key k only if absent and then return null; or do nothing
        // and return existing value
        public V putIfAbsent(K k, V v)
        {
            lock (this)
            {
                int h = getHash(k), hash = h % buckets.Length;
                ItemNode node = ItemNode.search(buckets[hash], k);
                if (node != null)
                    return node.v;
                else {
                    buckets[hash] = new ItemNode(k, v, buckets[hash]);
                    cachedSize++;
                    return null;
                }
            }
        }

        // Remove and return the value at key k if any, else return null
        public V remove(K k)
        {
            lock (this)
            {
                int h = getHash(k), hash = h % buckets.Length;
                ItemNode prev = buckets[hash];
                if (prev == null)
                    return null;
                else if (k.Equals(prev.k))
                {        // Delete first ItemNode
                    V old = prev.v;
                    cachedSize--;
                    buckets[hash] = prev.next;
                    return old;
                }
                else {                            // Search later ItemNodes
                    while (prev.next != null && !k.Equals(prev.next.k))
                        prev = prev.next;
                    // Now prev.next == null || k.Equals(prev.next.k)
                    if (prev.next != null)
                    {  // Delete ItemNode prev.next
                        V old = prev.next.v;
                        cachedSize--;
                        prev.next = prev.next.next;
                        return old;
                    }
                    else
                        return null;
                }
            }
        }

        // Iterate over the hashmap's entries one bucket at a time
        public void forEach(Action<K, V> consumer)
        {
            lock (this)
            {
                for (int hash = 0; hash < buckets.Length; hash++)
                {
                    ItemNode node = buckets[hash];
                    while (node != null)
                    {
                        consumer(node.k, node.v);
                        node = node.next;
                    }
                }
            }
        }

        // Double bucket table size, rehash, and redistribute entries.

        public void reallocateBuckets()
        {
            lock (this)
            {
                ItemNode[] newBuckets = new ItemNode[2 * buckets.Length];
                for (int hash = 0; hash < buckets.Length; hash++)
                {
                    ItemNode node = buckets[hash];
                    while (node != null)
                    {
                        int newHash = getHash(node.k) % newBuckets.Length;
                        ItemNode next = node.next;
                        node.next = newBuckets[newHash];
                        newBuckets[newHash] = node;
                        node = next;
                    }
                }
                buckets = newBuckets;
            }
        }

        class ItemNode
        {
            public readonly K k;
            public V v;
            public ItemNode next;

            public ItemNode(K k, V v, ItemNode next)
            {
                this.k = k;
                this.v = v;
                this.next = next;
            }

            public static ItemNode search(ItemNode node, K k)
            {
                while (node != null && !k.Equals(node.k))
                    node = node.next;
                return node;
            }
        }
    }

    // ----------------------------------------------------------------------
    // A hash map that permits thread-safe concurrent operations, using
    // lock striping (intrinsic locks on Objects created for the purpose).

    // NOT IMPLEMENTED: get, putIfAbsent, size, remove and forEach.

    // The bucketCount must be a multiple of the number lockCount of
    // stripes, so that h % lockCount == (h % bucketCount) % lockCount and
    // so that h % lockCount is invariant under doubling the number of
    // buckets in method reallocateBuckets.  Otherwise there is a risk of
    // locking a stripe, only to have the relevant entry moved to a
    // different stripe by an intervening call to reallocateBuckets.

    class StripedMap<K, V> : OurMap<K, V> where V : class
    {
        // Synchronization policy: 
        //   buckets[hash] is guarded by locks[hash%lockCount]
        //   sizes[stripe] is guarded by locks[stripe]
        private volatile ItemNode[] buckets;
        private readonly int lockCount;
        private readonly Object[] locks;
        private readonly int[] sizes;

        public StripedMap(int bucketCount, int lockCount)
        {
            if (bucketCount % lockCount != 0)
                throw new Exception("bucket count must be a multiple of stripe count");
            this.lockCount = lockCount;
            this.buckets = new ItemNode[bucketCount];
            this.locks = new Object[lockCount];
            this.sizes = new int[lockCount];
            for (int stripe = 0; stripe < lockCount; stripe++)
                this.locks[stripe] = new Object();
        }

        // Protect against poor hash functions and make non-negative
        private static int getHash(K k)
        {
            int kh = k.GetHashCode();
            return (kh ^ (kh >> 16)) & 0x7FFFFFFF;
        }

        // Return true if key k is in map, else false
        public bool containsKey(K k)
        {
            int h = getHash(k), stripe = h % lockCount;
            lock (locks[stripe])
            {
                int hash = h % buckets.Length;
                return ItemNode.search(buckets[hash], k) != null;
            }
        }

        // Return value v associated with key k, or null
        public V get(K k)
        {
            // TO DO: IMPLEMENT
            return null;
        }

        public int size()
        {
            // TO DO: IMPLEMENT
            return 0;
        }

        // Put v at key k, or update if already present 
        public V put(K k, V v)
        {
            int h = getHash(k), stripe = h % lockCount;
            lock (locks[stripe])
            {
                int hash = h % buckets.Length;
                ItemNode node = ItemNode.search(buckets[hash], k);
                if (node != null)
                {
                    V old = node.v;
                    node.v = v;
                    return old;
                }
                else {
                    buckets[hash] = new ItemNode(k, v, buckets[hash]);
                    sizes[stripe]++;
                    return null;
                }
            }
        }

        // Put v at key k only if absent
        public V putIfAbsent(K k, V v)
        {
            // TO DO: IMPLEMENT
            return null;
        }

        // Remove and return the value at key k if any, else return null
        public V remove(K k)
        {
            // TO DO: IMPLEMENT
            return null;
        }

        // Iterate over the hashmap's entries one stripe at a time; less locking
        public void forEach(Action<K, V> consumer)
        {
            // TO DO: IMPLEMENT
        }

        // First lock all stripes.  Then double bucket table size, rehash,
        // and redistribute entries.  Since the number of stripes does not
        // change, and since N = buckets.Length is a multiple of lockCount,
        // a key that belongs to stripe s because (getHash(k) % N) %
        // lockCount == s will continue to belong to stripe s.  Hence the
        // sizes array need not be recomputed.

        public void reallocateBuckets()
        {
            lockAllAndThen(delegate () {
                ItemNode[] newBuckets = new ItemNode[2 * buckets.Length];
                for (int hash = 0; hash < buckets.Length; hash++)
                {
                    ItemNode node = buckets[hash];
                    while (node != null)
                    {
                        int newHash = getHash(node.k) % newBuckets.Length;
                        ItemNode next = node.next;
                        node.next = newBuckets[newHash];
                        newBuckets[newHash] = node;
                        node = next;
                    }
                }
                buckets = newBuckets;
            });
        }

        // Lock all stripes, perform the action, then unlock all stripes
        private void lockAllAndThen(Action action)
        {
            lockAllAndThen(0, action);
        }

        private void lockAllAndThen(int nextStripe, Action action)
        {
            if (nextStripe >= lockCount)
                action();
            else
                lock (locks[nextStripe])
                {
                    lockAllAndThen(nextStripe + 1, action);
                }
        }

        class ItemNode
        {
            public readonly K k;
            public V v;
            public ItemNode next;

            public ItemNode(K k, V v, ItemNode next)
            {
                this.k = k;
                this.v = v;
                this.next = next;
            }

            // Assumes locks[hashCode(k) % lockCount] is held by the thread
            public static ItemNode search(ItemNode node, K k)
            {
                while (node != null && !k.Equals(node.k))
                    node = node.next;
                return node;
            }
        }
    }

    // ----------------------------------------------------------------------
    // A hashmap that permits thread-safe concurrent operations, using
    // lock striping (intrinsic locks on Objects created for the purpose),
    // and with immutable ItemNodes, so that reads do not need to lock at
    // all, only need visibility of writes, which is ensured through the
    // use of Volatile.Read(ref ...).

    // NOT IMPLEMENTED: get, putIfAbsent, size, remove and forEach.

    // The bucketCount must be a multiple of the number lockCount of
    // stripes, so that h % lockCount == (h % bucketCount) % lockCount and
    // so that h % lockCount is invariant under doubling the number of
    // buckets in method reallocateBuckets.  Otherwise there is a risk of
    // locking a stripe, only to have the relevant entry moved to a
    // different stripe by an intervening call to reallocateBuckets.

    class StripedWriteMap<K, V> : OurMap<K, V> where V : class
    {
        // Synchronization policy: an update to
        //   buckets[hash] is guarded by locks[hash % lockCount]
        //   sizes[stripe] is guarded by locks[stripe]
        // The buckets array elements must be accessed using
        // Volatile.{Read,Write} for visibility of writes to reads, because
        // although writes are guarded by the stripe's lock, we do read
        // buskets elements without locking.  The sizes array elements must
        // be accessed using atomic Interlocked operations, because although
        // writes are guarded by the stripe's lock, we do read sizes
        // elements without locking.  Unlike in Java we do not (indeed,
        // cannot) let buckets writes and reads piggyback on writes and
        // reads to the sizes element.
        private volatile ItemNode[] buckets;
        private readonly int lockCount;
        private readonly Object[] locks;
        private readonly int[] sizes;  // writes protected by locks[stripe], reads volatile

        public StripedWriteMap(int bucketCount, int lockCount)
        {
            if (bucketCount % lockCount != 0)
                throw new Exception("bucket count must be a multiple of stripe count");
            this.lockCount = lockCount;
            this.buckets = new ItemNode[bucketCount];
            this.locks = new Object[lockCount];
            this.sizes = new int[lockCount];
            for (int stripe = 0; stripe < lockCount; stripe++)
                this.locks[stripe] = new Object();
        }

        // Protect against poor hash functions and make non-negative
        private static int getHash(K k)
        {
            int kh = k.GetHashCode();
            return (kh ^ (kh >> 16)) & 0x7FFFFFFF;
        }

        // Return true if key k is in map, else false
        public bool containsKey(K k)
        {
            ItemNode[] bs = buckets;
            int h = getHash(k), hash = h % bs.Length;
            return ItemNode.search(Volatile.Read(ref bs[hash]), k, null);
        }

        // Return value v associated with key k, or null
        public V get(K k)
        {
            // TO DO: IMPLEMENT
            return null;
        }

        public int size()
        {
            // TO DO: IMPLEMENT
            return 0;
        }

        // Put v at key k, or update if already present.  The logic here has
        // become more contorted because we must not hold the stripe lock
        // when calling reallocateBuckets, otherwise there will be deadlock
        // when two threads working on different stripes try to reallocate
        // at the same time.
        public V put(K k, V v)
        {
            int h = getHash(k), stripe = h % lockCount;
            Holder old = new Holder();
            ItemNode[] bs;
            lock (locks[stripe])
            {
                bs = buckets;
                int hash = h % bs.Length;
                ItemNode node = Volatile.Read(ref bs[hash]),
                  newNode = ItemNode.delete(node, k, old);
                Volatile.Write(ref bs[hash], new ItemNode(k, v, newNode));
                // Increment if k was not already in map
                sizes[stripe] += newNode == node ? 1 : 0;
            }
            return old.get();
        }

        // Put v at key k only if absent.  The logic here has become more
        // contorted because we must not hold the stripe lock when calling
        // reallocateBuckets, otherwise there will be deadlock when two
        // threads working on different stripes try to reallocate at the
        // same time.
        public V putIfAbsent(K k, V v)
        {
            // TO DO: IMPLEMENT
            return null;
        }

        // Remove and return the value at key k if any, else return null
        public V remove(K k)
        {
            // TO DO: IMPLEMENT
            return null;
        }

        // Iterate over the hashmap's entries one stripe at a time.  
        public void forEach(Action<K, V> consumer)
        {
            // TO DO: IMPLEMENT
        }

        // First lock all stripes.  Then double bucket table size, rehash,
        // and redistribute entries.  Since the number of stripes does not
        // change, and since buckets.Length is a multiple of lockCount, a
        // key that belongs to stripe s because (getHash(k) % N) %
        // lockCount == s will continue to belong to stripe s.  Hence the
        // sizes array need not be recomputed.

        // VERY UNCLEAR whether it is necessary to make ALL accesses to
        // newBuckets elements Volatile, but better safe than sorry.

        public void reallocateBuckets()
        {
            lockAllAndThen(delegate {
                ItemNode[] bs = buckets,
                  newBuckets = new ItemNode[2 * buckets.Length];
                for (int hash = 0; hash < bs.Length; hash++)
                {
                    ItemNode node = Volatile.Read(ref bs[hash]);
                    while (node != null)
                    {
                        int newHash = getHash(node.k) % newBuckets.Length;
                        Volatile.Write(ref newBuckets[newHash],
                                       new ItemNode(node.k, node.v,
                                                         Volatile.Read(ref newBuckets[newHash])));
                        node = node.next;
                    }
                }
                buckets = newBuckets; // Visibility: buckets is volatile
            });
        }

        // Lock all stripes, perform action, then unlock all stripes
        private void lockAllAndThen(Action action)
        {
            lockAllAndThen(0, action);
        }

        private void lockAllAndThen(int nextStripe, Action action)
        {
            if (nextStripe >= lockCount)
                action();
            else
                lock (locks[nextStripe])
                {
                    lockAllAndThen(nextStripe + 1, action);
                }
        }

        class ItemNode
        {
            public readonly K k;
            public readonly V v;
            public readonly ItemNode next;

            public ItemNode(K k, V v, ItemNode next)
            {
                this.k = k;
                this.v = v;
                this.next = next;
                Thread.MemoryBarrier();
            }

            // These work on immutable data only, no synchronization needed.

            public static bool search(ItemNode node, K k, Holder old)
            {
                while (node != null)
                    if (k.Equals(node.k))
                    {
                        if (old != null)
                            old.set(node.v);
                        return true;
                    }
                    else
                        node = node.next;
                return false;
            }

            public static ItemNode delete(ItemNode node, K k, Holder old)
            {
                if (node == null)
                    return null;
                else if (k.Equals(node.k))
                {
                    old.set(node.v);
                    return node.next;
                }
                else {
                    ItemNode newNode = delete(node.next, k, old);
                    if (newNode == node.next)
                        return node;
                    else
                        return new ItemNode(node.k, node.v, newNode);
                }
            }
        }

        // Object to hold a "by reference" parameter.  For use only on a
        // single thread, so no need for "volatile" or synchronization.

        class Holder
        {
            private V value;
            public V get()
            {
                return value;
            }
            public void set(V value)
            {
                this.value = value;
            }
        }
    }

    // ----------------------------------------------------------------------
    // A wrapper around the .Net class library's
    // ConcurrentDictionary<K,V>, making it implement OurMap<K,V>.  But it
    // seems quite impossible to implement the put and putIfAbsent methods
    // from java.util.concurrent.ConcurrentMap correctly based on .Net's
    // ConcurrentDictionary.  This is related to this comment in the
    // documentation: "If you call AddOrUpdate simultaneously on different
    // threads, addValueFactory may be called multiple times, but its
    // key/value pair might not be added to the dictionary for every
    // call." from
    // https://msdn.microsoft.com/en-us/library/ee378675(v=vs.110).aspx
    // This comment curiously is missing from the other AddOrUpdate
    // overload, but probably is relevant there too.  The comment is found 
    // on one of the GetOrAdd overloads too.  

    // It would seem that TryUpdate would allow a CAS-style atomicity
    // strong enough to implement an analog of Java's putIfAbsent, but
    // this TryUpdate works only on keys present in the dictionary, so one
    // needs to handle the case of an absent key differently from a
    // present key, and it seems impossible to do this atomically.

    class WrapConcurrentHashMap<K, V> : OurMap<K, V> where V : class
    {
        private readonly ConcurrentDictionary<K, V> underlying
          = new ConcurrentDictionary<K, V>();

        public bool containsKey(K k)
        {
            return underlying.ContainsKey(k);
        }

        public V get(K k)
        {
            V result;
            return underlying.TryGetValue(k, out result) ? result : null;
        }

        // Implementation of Java's put semantics:
        public V put(K k, V v)
        {
            V oldV;
            while (underlying.TryGetValue(k, out oldV) ? !underlying.TryUpdate(k, v, oldV) : !underlying.TryAdd(k, v)) { }
            return oldV;
            // This does not give the Java put semantics because the delegate
            // may be called but its value ignored due to other threads
            // updating the same key:
            // V old = null;
            // underlying.AddOrUpdate(k, v, (oldK, oldV) => { old = oldV; return v; });
            // return old;
        }

        // Implementation of Java's putIfAbsent semantics:
        public V putIfAbsent(K k, V v)
        {
            V oldV;
            while (!underlying.TryGetValue(k, out oldV) && !underlying.TryAdd(k, v)) { }
            return oldV;
        }

        public V remove(K k)
        {
            V result;
            return underlying.TryRemove(k, out result) ? result : null;
        }

        public int size()
        {
            return underlying.Count;
        }

        public void forEach(Action<K, V> consumer)
        {
            foreach (KeyValuePair<K, V> kv in underlying)
                consumer(kv.Key, kv.Value);
        }

        public void reallocateBuckets() { }
    }

    // Crude timing utility ----------------------------------------

    public class Timer
    {
        private readonly System.Diagnostics.Stopwatch stopwatch
          = new System.Diagnostics.Stopwatch();
        public Timer() { Play(); }
        public double Check() { return stopwatch.ElapsedMilliseconds / 1000.0; }
        public void Pause() { stopwatch.Stop(); }
        public void Play() { stopwatch.Start(); }
    }

}
