﻿// Concurrent Hash Dictionary Test Framework - a framework for testing the Concurrent Hash Dictionary
// 
// Copyright(C) 2016 Edgars Ankorins & Mihai-Marius Vlasceanu
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace ConcurrentPerformanceTest
{

    public class DictionaryTest
    {
        /// <summary>
        /// The K -> V dictionary
        /// </summary>
        public ITestableDict<int, Entry> Dictionary { get; set; }

        /// <summary>
        /// Number of reads + writes
        /// </summary>
        public int NoOfOperations { get; set; }

        /// <summary>
        /// The Key space
        /// </summary>
        public int NoOfKeys { get; set; }

        /// <summary>
        /// Number of workers
        /// </summary>
        public int NoOfThreads { get; set; }

        /// <summary>
        /// Number of times to run each test
        /// </summary>
        public int NoOfRuns { get; set; }

        /// <summary>
        /// Read/write ratio
        /// </summary>
        public double ReadWriteRatio { get; set; }

        
        /// <summary>
        /// The key space array
        /// </summary>
        private List<int> keySpace;

        // Holds the run times for each run
        List<double> runTimes = new List<double>();

        // Holds the list of workers
        List<DictionaryWorker> workers = new List<DictionaryWorker>();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dictionary">The <K,V> pair dictionary</K></param>
        /// <param name="noOfOperations">Total number of reads and writes</param>
        /// <param name="noOfKeys">The K space</param>
        /// <param name="noOfThreads">Number of workers</param>
        /// <param name="noOfRuns">Number of runs for each test</param>
        /// <param name="readWriteRatio">Read/Write ratio</param>
        public DictionaryTest(TestSettings settings, int noOfThreads, double readWriteRatio)
        {
            Dictionary = settings.Dictionary;
            NoOfOperations = settings.NoOfOperations;
            NoOfKeys = settings.NoOfKeys;
            NoOfThreads = noOfThreads;
            NoOfRuns = settings.NoOfRuns;
            ReadWriteRatio = readWriteRatio;
            keySpace = settings.KeySpace.ToList();
        }

        /// <summary>
        /// Creates workers based on the number of threads
        /// </summary>
        /// <returns>A List of workers</returns>
        private List<DictionaryWorker> CreateWorkers()
        {
            // Number of operations per thread
            int noOfOperations = NoOfOperations / NoOfThreads;
            List<int> tempKeys = keySpace.Select(i => i).ToList();
            int[] overlappingKeys = this.overlappingKeys(tempKeys);
            workers = new List<DictionaryWorker>();
            for (int i = 0; i < NoOfThreads; i++)
            {

                // Calculate the range of the keyspace each worker will work on
                int rangeStart = (tempKeys.Count / NoOfThreads) * i;
                int rangeEnd = (tempKeys.Count / NoOfThreads) * (i + 1);

                List<int> keyRange = tempKeys.GetRange(rangeStart, rangeEnd - rangeStart);
                keyRange.AddRange(overlappingKeys);
                // Add to workers collection
                DictionaryWorker worker = new DictionaryWorker(Dictionary, keyRange.ToArray(), NoOfOperations / NoOfThreads, ReadWriteRatio, i, NoOfThreads);
                worker.ID = i;
                workers.Add(worker);
            }

            return workers;
        }

        /// <summary>
        /// Runs each test NoOfRuns times
        /// </summary>
        /// <returns>The average run time</returns>
        public double Run()
        {
            // Run the test NoOfRuns times
            for (int i = 0; i < NoOfRuns; i++)
            {
                Dictionary.Clear();
                populateDict();
                //string originalHash = Util.GetHash(Dictionary);

                // Generate the workers
                CreateWorkers();

                // Get the threads list to work on
                List<Thread> threads = workers.Select(w => new Thread(new ThreadStart(w.DoWork))).ToList();
                foreach (var item in threads)
                {
                    item.Name = String.Format("{0}, {1}-{2}({3})", Dictionary.GetType().Name, ReadWriteRatio, NoOfThreads, i);
                }
                // Start time for benchmarking
                Stopwatch timer = new Stopwatch();
                timer.Start();
                // Start all the threads
                threads.ForEach(t => t.Start());
                threads.ForEach(t => t.Join());

                // End time for benchmarking
                

                // Add the run time to the collection
                runTimes.Add(timer.ElapsedMilliseconds / 1000.0);

                int sum = threadOperations(workers).Sum(o => o);
                // Display data to console
                Util.w(string.Join("-", threadOperations(workers).Select(o => o.ToString()).ToArray()) + " " + sum);

                foreach(var item in workers)
                {
                    printWorkerOperationRatios(item);
                }

                //string afterHash = Util.GetHash(Dictionary);
                //Util.wf("The dictionary hashes are: \n Original: {0}\n Finished: {1}", originalHash, afterHash);
                Dictionary.Clear();
            }

            if(runTimes.Count() >= 5)
            {
                runTimes.Sort();
                double fastest = runTimes.First();
                double slowest = runTimes.Last();
                runTimes.Remove(fastest);
                runTimes.Remove(slowest);
            }
            return runTimes.Average();
        }


        public double ResizeTest()
        {
            int noOfInserts = NoOfOperations;
            for (int r = 0; r < NoOfRuns; r++)
            {

                var numThreads = Enumerable.Range(0, NoOfThreads);
                var noPerThread = noOfInserts/(NoOfThreads);
                var threads = numThreads.Select(t => new Thread(delegate()
                {
                    int startIndex = noPerThread*t;
                    int endIndex = noPerThread*(t + 1);
                    for (int j = startIndex; j < endIndex; j++)
                    {
                        Dictionary.AddOrUpdate(j, new Entry(j, t));
                    }
                })).ToList();
                for (int j = 0; j < threads.Count; j++)
                {
                    threads[j].Name = j.ToString();
                }

                Stopwatch timer = new Stopwatch();

                // Start all the threads
                threads.ForEach(t => t.Start());
                timer.Start();
                threads.ForEach(t => t.Join());

                // End time for benchmarking
                double time = timer.ElapsedMilliseconds;
                Dictionary.Clear();
                // Add the run time to the collection
                runTimes.Add(time / 1000.0);
                
            }
            return runTimes.Average();

        }
        /// <summary>
        /// Prints operations statistics for the referenced worker
        /// </summary>
        /// <param name="worker"></param>
        private void printWorkerOperationRatios(DictionaryWorker worker)
        {
            double total = worker.reads + worker.writes + worker.deletes;
            double readsDone = Math.Round(worker.reads / total, 4);
            double writesDone = Math.Round(worker.writes / total, 4);
            double deletesDone = Math.Round(worker.deletes / total, 4);

            string totalReads = String.Format("{0}/{1}", worker.failedReads, worker.reads);
            string totalWrites = String.Format("{0}/{1}", worker.failedWrites, worker.writes);
            string totalDeletes = String.Format("{0}/{1}", worker.failedDeletes, worker.deletes);
            Util.wf("Reads: {0}({1}), Writes:{2}({3}, Deletes: {4}({5})", readsDone, totalReads, writesDone, totalWrites, deletesDone, totalDeletes);
        }

        /// <summary>
        /// Populates the dictionary with <K,V>
        /// </summary>
        private void populateDict()
        {
           
            for(int i = 0; i < keySpace.Count; i++)
            {
                int key = keySpace[i];
                // Add entries to the dictionary before testing.
                // All values will have the format {thread_id}:{key}
                // Initialized to -1 as a placeholder until other threads modify the value
                Entry entry = new Entry();
                entry.ThreadId = -1;
                entry.Key = key;
                Dictionary.AddOrUpdate(key, entry);
            }
        }
        /// <summary>
        /// Returns an array of operations done by each worker
        /// </summary>
        /// <param name="workers"></param>
        /// <returns></returns>
        private int[] threadOperations(List<DictionaryWorker> workers)
        {
            int[] result = new int[workers.Count + 1];

            foreach (var item in workers)
            {
                for (int i = 0; i < item.threadOperations.Length; i++)
                {
                    result[i] += item.threadOperations[i];
                }
            }

            return result;
        }
        private int[] overlappingKeys(List<int> keys)
        {
            Random rand = new Random(1561416511);
            int noOfOverlappingKeys = (int)(keys.Count*0.05);
            int[]  overlappingKeys = new int[noOfOverlappingKeys];
            for (int i = 0; i < noOfOverlappingKeys; i++)
            {
                int index = rand.Next(keys.Count);
                overlappingKeys[i] = keys[index];
                keys.RemoveAt(index);
            }
            return overlappingKeys;
        }
    }
}
