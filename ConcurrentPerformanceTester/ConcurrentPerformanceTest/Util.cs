﻿// Concurrent Hash Dictionary Test Framework - a framework for testing the Concurrent Hash Dictionary
// 
// Copyright(C) 2016 Edgars Ankorins & Mihai-Marius Vlasceanu
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

using System;
using System.Security.Cryptography;
using System.Text;

namespace ConcurrentPerformanceTest
{
    /// <summary>
    /// Some utility methods
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// Shortcut for Console.Writeline
        /// </summary>
        /// <param name="message"></param>
        public static void w(string message)
        {
            Console.WriteLine(message);
        }

        public static void wf(string message, params object[] list)
        {
            w(string.Format(message, list));
        }

        /// <summary>
        /// Starts a timer for a specific worker op
        /// </summary>
        /// <param name="worker"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static DateTime startOperationTimer(DictionaryWorker worker, DictionaryWorker.TaskType type)
        {
            // Time to operation
            DateTime time = DateTime.Now;

            /*using (StreamWriter wr = new StreamWriter("logger.txt", append:true))
            {
                wr.WriteAsync(string.Format("Worker '{0}' starting a '{1}' opeation at '{2}'", worker.ID, type.ToString(), time.ToLongTimeString()));
            }*/

            return time;
        }

        /// <summary>
        /// Logs the time spent
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="worker"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static TimeSpan stopOperationTimer(DateTime startTime, DictionaryWorker worker, DictionaryWorker.TaskType type)
        {
            TimeSpan timeSpent = DateTime.Now - startTime;

            /*using (StreamWriter wr = new StreamWriter("logger.txt", append: true))
            {
                wr.WriteAsync(string.Format("Worker '{0}' has finished the '{1}' operation in '{2}'", worker.ID, type.ToString(), timeSpent.ToString()));
            }*/

            return timeSpent;
        }

        public static string GetHash(ITestableDict<int, Entry> source)
        {
            string hashString;

            try
            {
                string keyString = source.ToString();
                byte[] bytes = new byte[keyString.Length * sizeof(char)];
                Buffer.BlockCopy(keyString.ToCharArray(), 0, bytes, 0, bytes.Length);
                hashString = md5hash(bytes);
                return hashString;
            }
            catch (Exception e)
            {
                w(e.Message);
            }
            return null;
        }

        public static string md5hash(byte[] obj)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            try
            {
                byte[] result = md5.ComputeHash(obj);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < result.Length; i++)
                {
                    sb.Append(result[i].ToString("X2"));
                }
                return sb.ToString();
            }
            catch (ArgumentNullException ane)
            {
                w(ane.Message);
            }
            return null;
        }
    }
}