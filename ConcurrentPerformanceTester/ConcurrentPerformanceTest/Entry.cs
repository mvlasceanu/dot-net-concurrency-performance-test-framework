﻿// Concurrent Hash Dictionary Test Framework - a framework for testing the Concurrent Hash Dictionary
// 
// Copyright(C) 2016 Edgars Ankorins & Mihai-Marius Vlasceanu
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

/// <summary>
/// Entry object for each dictionary item
/// </summary>
public class Entry
{
    /// <summary>
    /// A unique ID for each instance
    /// </summary>
    public int ThreadId { get; set; }

    /// <summary>
    /// They key entry
    /// </summary>
    public int Key { get; set; }

    public Entry() { }
    public Entry(int thread, int key)
    {
        ThreadId = thread;
        this.Key = key;
    }
    /// <summary>
    /// String representation of this instance
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
        return ThreadId + ":" + Key;
    }
}