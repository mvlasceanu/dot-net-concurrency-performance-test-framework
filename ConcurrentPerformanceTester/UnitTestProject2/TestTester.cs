﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using C5.Concurrent;
using ConcurrentPerformanceTest;
using System;
using ConcurrentPerformanceTester;
using System.Linq;
using System.Threading;

namespace UnitTests
{
    [TestClass]
    public class TestTester
    {
        /// <summary>
        /// Tests the different operations on the dictionary
        /// </summary>
        [TestMethod]
        public void testOperationsOnEmptyDictionary()
        {
            HashDictionary<int, Entry> dictionary = new HashDictionary<int, Entry>();
            HashDictionaryTestable testable = new HashDictionaryTestable(dictionary);
            TestSettings settings = new TestSettings(testable, 7, 5, 10, 5);

            List<TestRun> ourTestRuns = runTests(settings);

            Assert.AreEqual(0, dictionary.Count());
            Assert.IsFalse(dictionary.ContainsKey(117));
            Assert.IsNull(dictionary.Get(117));
            Assert.IsNotNull(dictionary.AddOrUpdate(117, new Entry { ThreadId = -1, Key = 117 }));
            Assert.IsTrue(dictionary.ContainsKey(117));
            Assert.IsNotNull(dictionary.AddOrUpdate(17, new Entry { ThreadId = -1, Key = 17 }));
            Assert.AreEqual(2, dictionary.Count());
            Assert.IsTrue(dictionary.ContainsKey(17));
            Assert.IsTrue(dictionary.ContainsKey(117));
            Assert.AreEqual(2, dictionary.Count());
            Assert.IsTrue(dictionary.Delete(117));
            Assert.IsFalse(dictionary.ContainsKey(117));
            Assert.IsNull(dictionary.Get(117));
            Assert.AreEqual(1, dictionary.Count());
            Assert.IsFalse(dictionary.AddOrUpdate(17, new Entry { ThreadId = 1, Key = 17 }).Equals(new Entry { ThreadId = -1, Key = 17}));
            Assert.AreEqual(1, dictionary.Count());
            Assert.IsTrue(dictionary.ContainsKey(17));
            Assert.IsNotNull(dictionary.AddOrUpdate(217, new Entry { ThreadId = -1, Key = 27}));
            Assert.AreEqual(2, dictionary.Count());
            Assert.IsTrue(dictionary.ContainsKey(217));
            Assert.IsNotNull(dictionary.AddOrUpdate(34, new Entry { ThreadId = -1, Key = 34}));
            Assert.AreEqual(3, dictionary.Count());
            Assert.IsTrue(dictionary.Delete(34));
            Assert.IsFalse(dictionary.ContainsKey(34));
            Assert.AreEqual(2, dictionary.Count());
        }

        /// <summary>
        /// Tests writes operations only where keys should remain the same
        /// but the values must change
        /// </summary>
        public void testOnlyWrites()
        {
            HashDictionary<int, Entry> dictionary = new HashDictionary<int, Entry>();
            dictionary.AddOrUpdate(117, new Entry { ThreadId = -1, Key = 117});
            dictionary.AddOrUpdate(17, new Entry { ThreadId = -1, Key = 17});
            dictionary.AddOrUpdate(34, new Entry { ThreadId = -1, Key = 34});
            dictionary.AddOrUpdate(217, new Entry { ThreadId = -1, Key = 27});

            HashDictionaryTestable testable = new HashDictionaryTestable(dictionary);

            TestSettings settings = new TestSettings(testable, 7, 5, 10, 5);

            List<TestRun> ourTestRuns = runTests(settings);

            Assert.AreEqual(4, dictionary.Count());

            // 117
            Assert.IsTrue(dictionary.ContainsKey(117));
            Assert.IsNotNull(dictionary.Get(117));
            Assert.IsTrue(dictionary.Get(117).ThreadId > -1);
            Assert.IsTrue(dictionary.Get(117).Key == 117);

            // 17
            Assert.IsTrue(dictionary.ContainsKey(17));
            Assert.IsNotNull(dictionary.Get(17));
            Assert.IsTrue(dictionary.Get(17).ThreadId > -1);
            Assert.IsTrue(dictionary.Get(17).Key == 17);

            // 34
            Assert.IsTrue(dictionary.ContainsKey(34));
            Assert.IsNotNull(dictionary.Get(34));
            Assert.IsTrue(dictionary.Get(34).ThreadId > -1);
            Assert.IsTrue(dictionary.Get(34).Key == 34);

            // 217
            Assert.IsTrue(dictionary.ContainsKey(217));
            Assert.IsNotNull(dictionary.Get(217));
            Assert.IsTrue(dictionary.Get(217).ThreadId > -1);
            Assert.IsTrue(dictionary.Get(217).Key == 217);
        }

        public static List<TestRun> runTests(TestSettings settings)
        {
            return DictionaryTestFactory.RunComprehensiveTests(settings);
        }

        //[TestMethod]
        public void testReproductability()
        {
            HashDictionary<int, Entry> dictionary = new HashDictionary<int, Entry>();
            dictionary.AddOrUpdate(117, new Entry { ThreadId = -1, Key = 117});
            dictionary.AddOrUpdate(17, new Entry { ThreadId = -1, Key = 17});
            dictionary.AddOrUpdate(34, new Entry { ThreadId = -1, Key = 34});
            dictionary.AddOrUpdate(217, new Entry { ThreadId = -1, Key = 27});
            HashDictionaryTestable testable = new HashDictionaryTestable(dictionary);
            HashDictionaryTestable clone = new HashDictionaryTestable(dictionary);
            TestSettings settings = new TestSettings(testable, 7, 5, 4, 5);

            List<TestRun> tests = runTests(settings);

            Assert.AreEqual(clone, testable);
        }

        public static void Main(string[] args)
        {
            TestTester t = new TestTester();
            t.testReproductability();
            Console.ReadLine();
        }

        /// <summary>
        /// Returns the number before the : in a string like -1:17
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static int getWorkersCount(string value)
        {
            string[] data = value.Split(':');
            int id;
            if (int.TryParse(data[0], out id))
                return id;
            else
                return -2;
        }
    }
}
