﻿using System;
using C5.Concurrent;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject2
{
    [TestClass]
    public class HashDictionaryTests
    {

        [TestMethod]
        public void TestAddOrUpdate()
        {
            HashDictionary<int,string> dict = new HashDictionary<int, string>();
            Assert.AreEqual(0, dict.Count());
            string first = dict.AddOrUpdate(1, "0:1");
            Assert.IsNotNull(first);
            Assert.AreEqual(first, "0:1");
            string second = dict.AddOrUpdate(2, "0:2");
            Assert.AreEqual(2, dict.Count());
            Assert.IsNotNull(second);
            Assert.AreEqual(second, "0:2");
            string third = dict.AddOrUpdate(1, "1:1");
            Assert.AreEqual(2, dict.Count());

           
            Assert.IsNotNull(third);
            Assert.AreEqual(third, first);
        }

        [TestMethod]
        public void TestTryRemove()
        {
            HashDictionary<int, string> dict = new HashDictionary<int, string>();
            Assert.AreEqual(0, dict.Count());
            string first = dict.AddOrUpdate(1, "0:1");
            string second = dict.AddOrUpdate(2, "0:2");
            Assert.AreEqual(2, dict.Count());

            string v = "";
            bool removed = dict.TryRemove(1, out v);

            Assert.IsTrue(removed);
            Assert.AreEqual(v, first);
            Assert.AreEqual(1, dict.Count());
        }
    }
}
