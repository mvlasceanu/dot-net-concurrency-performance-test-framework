﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C5.Concurrent;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject2
{
    [TestClass]
    public class HashDictionaryTest
    {

        [TestMethod]
        public void testOperationsOnEmptyDictionary()
        {
            HashDictionary<int, string> dict = new HashDictionary<int, string>();

            for (int i = 0; i < 10; i++)
            {
                dict.AddOrUpdate(i * 2, i.ToString());
                int count = dict.Count();
                Assert.IsTrue(Equals(count, i + 1));
            }
            string o = "";

            dict.TryRemove(16, out o);
            Assert.IsTrue(dict.Count() == 9);
            Assert.IsFalse(dict.ContainsKey(16));
            dict.AddOrUpdate(16, "16");
            Assert.IsTrue(dict.Count() == 10);
            Assert.IsTrue(dict.Keys.Length == 10);
            for (int i = 0; i < 10; i++)
            {
                dict.TryRemove(i * 2, out o);
                Assert.IsTrue(dict.Count() == 10 - (i + 1));
                Assert.IsFalse(dict.ContainsKey(i *2));
            }
            
        }
    }
}
