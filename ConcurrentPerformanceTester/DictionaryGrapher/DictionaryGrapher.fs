﻿// Concurrent Hash Dictionary Test Framework - a framework for testing the Concurrent Hash Dictionary
// 
// Copyright(C) 2016 Edgars Ankorins & Mihai-Marius Vlasceanu
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

namespace DictionaryGrapher
open FSharp.Charting
open ConcurrentPerformanceTest
open System.Collections.Generic
open System
module Grapher =
    let ratioToString ratio = 
        let percentage = ratio * 100.0
        let read = percentage
        let write = (100.0 - percentage)
        String.Format("{0}% reads {1}% writes", read, write)
    let graph (results : List<ConcurrentPerformanceTest.TestRun>) = 
        let path = @"..\..\..\..\ConcurrentPerformanceTester\images"
        let title = results.Item(0).DictionaryType
        let runs = results |> Seq.map(fun i -> i.ReadWriteRatio)
        let dataRuns = results |> Seq.map(fun i -> i.TestResults |> Seq.map(fun j -> j.NoOfThreads, j.AverageRunTime))
        let annotatedDataRuns = Seq.zip runs dataRuns
        let graphs = annotatedDataRuns |> Seq.map(fun (ratio, data) -> Chart.Line(data, Name=ratioToString ratio, Title=ratioToString ratio))
        let finalChart = Chart.Combine(graphs)
        finalChart.WithYAxis(Min = 0.0, Max = 5.0) |> ignore
        finalChart.WithLegend(Enabled = true) |> ignore
        finalChart.WithTitle(Text=title) |> ignore
        finalChart.WithDataPointLabels(LabelPosition = ChartTypes.LabelPosition.Center) |> ignore

        let today = DateTime.Now
        let chartForm = finalChart.ShowChart()
        finalChart.SaveChartAs(String.Format("{0}\{1}_{2}.{3}", path, today.ToFileTimeUtc(), title, "bmp"), ChartTypes.ChartImageFormat.Bmp)
        chartForm.Close()
        0
