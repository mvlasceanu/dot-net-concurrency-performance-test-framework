﻿// Concurrent Hash Dictionary Test Framework - a framework for testing the Concurrent Hash Dictionary
// 
// Copyright(C) 2016 Edgars Ankorins & Mihai-Marius Vlasceanu
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

using System.IO;
using ConcurrentPerformanceTest;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using C5.Concurrent;
using DictionaryGrapher;
using System.Threading;
using System.Diagnostics;

namespace ConcurrentPerformanceTester
{
    /// <summary>
    /// Entry Point 
    /// </summary>
    class Program
    {
        /// <summary>
        /// Entry point method
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
        //    while (true)
        //    {
        //        TestStuff();
        //    }
            ConfigurationSettings settings = readXml("Settings.xml");
            Console.ReadLine();
            List<ITestableDict<int, Entry>> testables = new List<ITestableDict<int, Entry>>();
            ITestableDict<int, Entry> synchronizedDict = new HashDictionaryTestable(new SynchronizedHashDictionary<int, Entry>());
            ITestableDict<int, Entry> stripedDict = new HashDictionaryTestable(new HashDictionary<int, Entry>());
            ITestableDict<int, Entry> fullStripedDict = new HashDictionaryTestable(new FullyStripedHashDictionary<int, Entry>());
            ITestableDict<int, Entry> casDict = new HashDictionaryTestable(new CasDictionary<int, Entry>());
            ITestableDict<int, Entry> concurrentDict = new ConcurrentDictionaryTestable(new ConcurrentDictionary<int, Entry>());

            testables.Add(synchronizedDict);
            testables.Add(stripedDict);
            testables.Add(fullStripedDict);
            testables.Add(concurrentDict);
            testables.Add(casDict);

            var testMaps = testables.Where(i => settings.TestMaps.Any(j => i.DictName.StartsWith(j))).ToList();

            foreach (var setting in settings.RunSettings)
            {
                foreach (var map in testMaps)
                {
                    TestSettings testSettings = new TestSettings(map, setting.NoOfThreads, setting.NoOfOperations, setting.NoOfKeys, setting.NoOfRuns);
                    switch (setting.RunName)
                    {
                        case "Comprehensive":
                            RunTests(testSettings);
                            break;
                        case "ReadOnly":
                            RunReadOnlyTest(testSettings);
                            break;
                        case "WriteOnly":
                            RunWriteOnlyTest(testSettings);
                            break;
                        case "Resize":
                            RunResizeTests(testSettings);
                            break;

                    }
                }
            }

            Console.ReadLine();
        }

        private static void TestStuff()
        {
            ConcurrentQueue<Chunk> queue = new ConcurrentQueue<Chunk>();
            int start = 2048;
            CasDictionary<int, string>.Node[] buckets = new CasDictionary<int, string>.Node[start];
            double singleResize = 0.0;
            double chunkResize = 1.0;
            Stopwatch timer = new Stopwatch();

            for (int z = 0; singleResize <= chunkResize; start = start << 1)
            {
                Thread.MemoryBarrier();
                buckets = new CasDictionary<int, string>.Node[start];
                for (int i = 0; i < buckets.Length; i++)
                {
                    int index = getIndex(i, buckets.Length);
                    buckets[index] = new CasDictionary<int, string>.Node(i, i.ToString(), null);
                }
                CasDictionary<int, string>.Node[] newBuckets = new CasDictionary<int, string>.Node[start << 1];

                //Single Thread Test
                timer.Start();
                for (int i = 0; i < buckets.Length; i++)
                {
                    int index = getIndex(i, newBuckets.Length);
                    newBuckets[index] = buckets[i];
                }
                singleResize = timer.ElapsedTicks;
                //Single Thread Test End
                timer.Reset();
                newBuckets = new CasDictionary<int, string>.Node[start << 1];

                int quarter = buckets.Length / 4;
                timer.Start();
                for (int i = 0; i < 4; i++)
                {
                    Chunk chunk = new Chunk(i * quarter, quarter * (i + 1), buckets);
                    queue.Enqueue(chunk);
                }
                double stuff = timer.ElapsedTicks;
                int threadsInitiated = 0;
                bool startThreads = false;
                Barrier barrier = new Barrier(4, (bar) => {
                    if (threadsInitiated == 4)
                    {
                        startThreads = true;
                        timer.Start();
                    }
                });
                timer.Reset();
                var threads = Enumerable.Range(1, 4).Select(i => new Thread(delegate ()
                {
                    Interlocked.Increment(ref threadsInitiated);
                    barrier.SignalAndWait();
                    while (!startThreads) { }

                    Chunk chunk;

                    queue.TryDequeue(out chunk);
                    CasDictionary<int, string>.Node[] bucketChunk;
                    for (int j = chunk.start; j < chunk.end; j++)
                    {
                        int index = getIndex(j, newBuckets.Length);
                        newBuckets[index] = chunk.buckets[j];
                    }
                })).ToList();
                
                threads.ForEach(t => t.Start());
                double threadStartingTime = timer.ElapsedTicks;
                threads.ForEach(t => t.Join());
                chunkResize = timer.ElapsedTicks;
                timer.Reset();
                Thread.MemoryBarrier();

            }
            double difference = singleResize - chunkResize;
            Console.WriteLine(difference + "    " + start);
        }
        private static int getIndex(int k, int size)
        {
            int hv = getHashKey(k);
            return hv % size;
        }
        private static int getHashKey(int k)
        {
            int kh = k.GetHashCode();
            return (kh ^ (kh >> 16)) & 0x7FFFFFF;
        }
        private static void RunTests(TestSettings settings)
        {
            List<TestRun> tests = runTests(settings);
            writeFile(tests);
            Grapher.graph(tests);
        }
        private static void RunReadOnlyTest(TestSettings settings)
        {
            List<TestRun> tests = DictionaryTestFactory.RunReadsTest(settings);
            writeFile(tests);
            //Grapher.graph(tests);
        }

        private static void RunWriteOnlyTest(TestSettings settings)
        {
            List<TestRun> tests = DictionaryTestFactory.RunWritesTest(settings);
            writeFile(tests);
            //Grapher.graph(tests);
        }

        private static void RunResizeTests(TestSettings settings)
        {
            List<TestRun> tests = DictionaryTestFactory.RunResizeTest(settings);
            writeFile(tests);
            Grapher.graph(tests);
        }
        /// <summary>
        /// Starts comprehensive tests
        /// </summary>
        /// <param name="settings">Test parameters</param>
        /// <returns>A list of the test runs results</returns>
        public static List<TestRun> runTests(TestSettings settings)
        {
            return DictionaryTestFactory.RunComprehensiveTests(settings);
        }

        /// <summary>
        /// Creates a CSV file with the results
        /// </summary>
        /// <param name="testRuns"></param>
        public static void writeFile(List<TestRun> testRuns)
        {
            foreach (var item in testRuns)
            {
                Console.WriteLine(item.ToString());
                foreach (var result in item.TestResults)
                {
                    Console.WriteLine(result.ToString());
                }
            }

            using (StreamWriter writetext = new StreamWriter(DateTime.Now.ToFileTimeUtc() + "_" + testRuns.First().DictionaryType + "_output.txt"))
            {
                foreach (var item in testRuns)
                {
                    writetext.WriteLine(item.ToCSV());
                }
            }
        }

        private static ConfigurationSettings readXml(string file)
        {
            string data = File.ReadAllText(file);
            ConfigurationSettings settings = null;
            XmlSerializer serializer = new XmlSerializer(typeof(ConfigurationSettings));
            
            using (StreamReader reader = new StreamReader(file))
            {
                settings = (ConfigurationSettings)serializer.Deserialize(reader);
            }

            return settings;
        }
    }
    public class Chunk
    {
        internal int start;
        internal int end;
        internal CasDictionary<int, string>.Node[] buckets;

        public Chunk(int start, int end, CasDictionary<int, string>.Node[] buckets)
        {
            this.start = start;
            this.end = end;
            this.buckets = buckets;
        }
    }
}
