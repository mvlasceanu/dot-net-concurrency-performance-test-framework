﻿// Concurrent Hash Dictionary Test Framework - a framework for testing the Concurrent Hash Dictionary
// 
// Copyright(C) 2016 Edgars Ankorins & Mihai-Marius Vlasceanu
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.If not, see<http://www.gnu.org/licenses/>.

using C5.Concurrent;
using ConcurrentPerformanceTest;
using System;
using System.Text;

namespace ConcurrentPerformanceTester
{
    /// <summary>
    /// The testable HashDictionary
    /// </summary>
    [Serializable]
    public class HashDictionaryTestable : ITestableDict<int, Entry>
    {
        /// <summary>
        /// Holds the dictionary instance
        /// </summary>
        public C5.Concurrent.HashDictionary.IDictionary<int, Entry> dict;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dict"></param>
        public HashDictionaryTestable(C5.Concurrent.HashDictionary.IDictionary<int, Entry> dict)
        {
            this.dict = dict;
        }
        string ITestableDict<int, Entry>.DictName
        {
            get { return dict.GetType().Name; }
        }
        /// <summary>
        /// Get Entry at key K
        /// </summary>
        /// <param name="k">Key to get the Entry at</param>
        /// <returns>The Entry object</returns>
        public Entry Get(int k)
        {
            Entry o;
            if (dict.TryGetValue(k, out o))
            {
                return o;
            }
            return null;
        }

        /// <summary>
        /// Add Entry at key K or update it
        /// </summary>
        /// <param name="k">Key to Add/Update</param>
        /// <param name="v">Entry to be added/changed</param>
        /// <returns>The updated Entry</returns>
        public Entry AddOrUpdate(int k, Entry v)
        {
            return dict.AddOrUpdate(k, v);
        }

        /// <summary>
        /// Delete the entry at key K
        /// </summary>
        /// <param name="k">Key to be deleted</param>
        /// <param name="v">The out Entry to pass the deleted entry to</param>
        /// <returns>Has the Entry been deleted?</returns>
        public bool Delete(int k, out Entry v)
        {
            Entry o;
            bool removed = dict.TryRemove(k, out o);
            v = o;
            return removed;

        }

        /// <summary>
        /// ContainsKey method
        /// </summary>
        /// <param name="k">Key to be looked for</param>
        /// <returns>Does the dictionary contain key K</returns>
        public bool ContainsKey(int k)
        {
            return dict.ContainsKey(k);
        }

        /// <summary>
        /// Empty the dictionary
        /// </summary>
        public void Clear()
        {
            dict.Clear();
        }

        /// <summary>
        /// List the keys in the dictionary
        /// </summary>
        /// <returns>An array containing all the keys in the dictionary</returns>
        public int[] Keys()
        {
            return dict.Keys;
        }

        /// <summary>
        /// Count the items in the dictionary
        /// </summary>
        /// <returns>An integer representing the number of entries</returns>s
        public int Count()
        {
            return dict.Count();
        }

        /// <summary>
        /// The string representation of this instance
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (int key in Keys())
            {
                sb.Append(key);
                sb.Append(" => ");
                sb.Append(Get(key));
                sb.Append(",");
                sb.Append("\n");
            }

            return sb.ToString();
        }
    }
}
