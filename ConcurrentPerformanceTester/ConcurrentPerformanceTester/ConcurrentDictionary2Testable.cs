﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConcurrentPerformanceTest;

namespace ConcurrentPerformanceTester
{
    public class ConcurrentDictionary2Testable : ITestableDict<int, Entry>
    {
        ConcurrentDictionary2<int, Entry> dict = new ConcurrentDictionary2<int, Entry>();
        public ConcurrentDictionary2Testable(ConcurrentDictionary2<int, Entry> dict)
        {
            this.dict = dict;
        }
        string ITestableDict<int, Entry>.DictName
        {
            get { return dict.GetType().Name; }
        }

        public Entry Get(int k)
        {
            Entry o;
            if (dict.TryGetValue(k, out o))
            {
                return o;
            }
            return null;
        }

        public Entry AddOrUpdate(int k, Entry v)
        {
            return dict[k] = v;
        }

        public bool Delete(int k, out Entry v)
        {
            Entry o;
            bool removed = dict.TryRemove(k, out o);
            v = o;
            return removed; ;

        }

        public bool ContainsKey(int k)
        {
            return dict.ContainsKey(k);
        }

        public void Clear()
        {
            dict.Clear();
        }

        public int[] Keys()
        {
            return dict.Keys.ToArray();
        }

        public int Count()
        {
            return dict.Count;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (int key in Keys())
            {
                sb.Append(key);
                sb.Append(" => ");
                sb.Append(Get(key));
                sb.Append(",");
                sb.Append("\n");
            }

            return sb.ToString();
        }
    }
}
