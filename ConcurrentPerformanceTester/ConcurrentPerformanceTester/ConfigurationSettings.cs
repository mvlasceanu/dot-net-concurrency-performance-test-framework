﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConcurrentPerformanceTester
{
    [Serializable]
    [XmlRoot("ConfigurationSettings")]
    public class ConfigurationSettings
    {
        [XmlArray(ElementName = "TestMaps")]
        [XmlArrayItem("TestMap")]
        public string[] TestMaps { get; set; }

        [XmlArray(ElementName = "RunSettings")]
        [XmlArrayItem("RunSetting")]
        public RunSettings[] RunSettings { get; set; }
        
    }
    [Serializable]
    public class RunSettings
    {
        [XmlElement(ElementName = "RunName")]
        public string RunName { get; set; }
        [XmlElement(ElementName = "NoOfThreads")]
        public int NoOfThreads { get; set; }
        [XmlElement(ElementName = "NoOfRuns")]
        public int NoOfRuns { get; set; }
        [XmlElement(ElementName = "NoOfKeys")]
        public int NoOfKeys { get; set; }
        [XmlElement(ElementName = "NoOfOperations")]
        public int NoOfOperations { get; set; }
    }
}
